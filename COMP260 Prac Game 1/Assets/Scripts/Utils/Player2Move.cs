﻿using UnityEngine;
using System.Collections;

public class Player2Move : MonoBehaviour {

	public float maxSpeed = 5.0f;

	public Vector2 velocity2;

	// Update is called once per frame
	void Update () {



		Vector2 direction2; 
		direction2.x = Input.GetAxis ("Horizontal2");
		direction2.y = Input.GetAxis ("Vertical2");	


		Vector2 velocity2 = direction2 * maxSpeed;

		transform.Translate (velocity2 * Time.deltaTime);
	}
}
